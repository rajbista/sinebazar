const express = require('express');
const mongoose = require('mongoose');
const Joi = require('joi');
const morgan = require('morgan');
const helmet = require('helmet');
const config =  require('config');
const startupDebugger = require('debug')('app:startup');
const customers = require('./routes/customer');
const genres = require('./routes/genres');
const movies = require('./routes/movies');

const app = express();
app.use(express.json());
app.use(express.static('public'));

// Helps to secure app by setting various HTTP headers
app.use(helmet());

// Configuration
console.log('Application Name: ' + config.get('name'));
console.log('Mail Server' + config.get('mail.host'));
console.log('Mail Password:' + config.get('mail.password'));

if(app.get('env') === 'development'){
    app.use(morgan('tiny'));
    startupDebugger('Morgan is enabled....');    
}

// Database connection
mongoose.connect('mongodb://localhost/sinebazar')
    .then(() => { console.log('Database is connected...')})
    .catch(err => console.log('Failed to databse connection...'));


// Router for genres
app.use('/api/genres', genres);

// Router for customers
app.use('/api/customers', customers);

// Router for Movies
app.use('/api/movies', movies);


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`server is running on ${port}`));