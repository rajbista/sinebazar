const mongoose = require('mongoose');
const Joi = require('joi');

// Schema for Genres
const genreSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true, 
        minlength: 3,
        maxlength: 255
    }
});
const Genres = mongoose.model('Genres', genreSchema);

// Validation for Genres
function Validate(genre) {
    const schema = {
      name: Joi.string().min(3).required()
    };
    return Joi.validate(genre, schema);
  }

module.exports = {Genres, Validate, genreSchema};
