const mongoose = require('mongoose');
const Joi = require('joi');
const {genreSchema} = require('../model/genres');

// Database Schema for Genres
const Movie = mongoose.model('Movie', new mongoose.Schema({
    title: {
        type: String, 
        required: true, 
        minlength: 3,
        maxlength: 50,
        trim: true
    },
    genre: {
        type: genreSchema,
        required: true
    },
    numberInStock:{
        type: Number,
        required: true,
        min: 0,
        max: 255
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    }
}));

// Validation for Genres
function ValidateMovie(movie) {
    const schema = {
      title: Joi.string().min(3).max(50).required(),
      genreId: Joi.string().required(),
      numberInStock: Joi.number().min(0).required(),
      dailyRentalRate: Joi.number().min(0).required()
    };
    return Joi.validate(movie, schema);
  }

module.exports = {Movie, ValidateMovie};
