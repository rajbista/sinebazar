const mongoose = require('mongoose');
const Joi = require('joi');

// Database Schema
const Customer = mongoose.model('Customer', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    phone: {
        type: Number, 
        required: true, 
        min: 5,
    },
    isGold: {
        type: Boolean, 
        required: true,
        default: false
    }
}));

// Validation Customer
function Validate(customer){
    const schema ={
        name: Joi.string().min(3).required(),
        phone: Joi.string().min(10).required(),
        isGold: Joi.boolean().required()
    };
    return Joi.validate(customer, schema);
}

module.exports= {Customer, Validate};