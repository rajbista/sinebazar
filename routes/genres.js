const express = require('express');
const Joi = require('joi');
const {Genres, Validate} = require('../model/genres');

const router = express();

router.get('/', async (req, res) =>{
    try{
        const genres = await Genres.findOne();
        res.send(genres);
    }
    catch(err){
        res.send(err.message);
    }
});

router.post('/', async (req, res) =>{
    const {error} = Validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const genre = new Genres({
        name: req.body.name
      });
      try{
        await genre.save();
        res.send(genre);
      }
      catch(err){
          res.send(err.message);
      }
});

router.put('/:id', async (req, res) => {
    const { error } = Validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);
    
    try{
        const genre = await Genres.findByIdAndUpdate(req.params.id, { name: req.body.name }, {new: true});
        if (!genre) return res.status(404).send('The genre with the given ID was not found.');
        res.send(genre);
    }
    catch(err){
        res.send(err.message);
    }
  });

  router.delete('/:id',async (req, res) => {
    const { error } = Validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);

    try{
        const genre = await Genres.findByIdAndRemove(req.params.id);
        if (!genre) return res.status(404).send('The genre with the given ID was not found.');
  
        res.send(genre);
    }
    catch(err){
        res.send(err.message);
    }
  });
  
  router.get('/:id',async (req, res) => {
      try{
        const genre = await Genres.findOne({_id: req.params.id});
        if (!genre) return res.status(404).send('The genre with the given ID was not found.');
    
        res.send(genre);
      }
      catch(err){
        res.send(err.message);
      }
  });

  module.exports = router;