const express = require('express');
const Joi = require('joi');
const {Genres} = require('../model/genres');
const {Movie, ValidateMovie} = require('../model/movie');

const router = express.Router();

router.get('/',async (req, res) =>{
    try{
        const movies = await Movie.find().sort('name');
        res.send(movies);
    }
    catch(err){
        res.send(err.message);
    }
});

// Post Router
router.post('/',async (req, res) => {
    const {error} = ValidateMovie(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    const genre = Genres.findById(req.body._id);
    if(!genre) return res.status(400).send('Invalid genre!');
    const movie = new Movie({
        title: req.body.title,
        genre: {
            _id: genre._id,
            name: genre.name
        },
        numberInStock: req.body.numberInStock,
        dailyRentalRate: req.body.dailyRentalRate
    });
    try{
        await movie.save();
        res.send(movie);
    }
    catch(err){
        res.send(err.message);
    }
});

module.exports = router;
