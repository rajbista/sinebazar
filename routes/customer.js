const express = require('express');
const Joi = require('joi');
const {Customer, Validate} = require('../model/customer');

const router = express.Router();

router.get('/',async (req, res) =>{
    try{
        const customer = await Customer.find();
        res.send(customer);
    }
    catch(err){
        res.send(err.message);
    }
});

// Post Router
router.post('/',async (req, res) => {
    const {error} = Validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    const customer = new Customer(req.body);
    try{
        await customer.save();
        res.send(customer);
    }
    catch(err){
        res.send(err.message);
    }
});

module.exports = router;
